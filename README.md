This repositories has the following :
1- Kalman filter and FTSP implementation on top of modified Atmel MAC software package.

2- RF233 oscilloscope interrupts files to time mark frame transmission and reception.
     A- Rising edge for starting of processing at TX and RX.
     B- Falling edge for end of processing at TX and RX.
     C- RX to RX files

3- Beagle Bone Black image to expose HW 32bit capture
Andrew Symington used the pps-gmtimer patch to enable this feature in the userspace
https://github.com/ddrown/pps-gmtimer

Here are some tips to play with the timer
username : root
password : roseline

#ethernet up
Ifup eth0

#timer 4 will work on external clock pin (P9.41)
cp /home/debian/DD-GPS-TCLKIN-00A0.dtbo /lib/firmware/DD-GPS-00A0.dtbo
reboot

#timer 4 will work on internal clock 24 Mhz
cp /home/debian/DD-GPS-00A0.dtbo /lib/firmware/
reboot

#to see the new patch capabilities
cd /sys/devices/ocp.3/pps_gmtimer.*

# to read count at interrupt
more count_at_interrupt

# to read current timer 4 value
more timer_counter

connections are
P9.41 is for  TCLKIN
P8.7   is for Capture inputs
P9.01 GND

# this perl script will print on the screen any new value in the "count_at_interrupt",
perl /home/debian/cap_perl